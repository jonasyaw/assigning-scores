from modules.getting_docs import get_docs
from modules.writing_marks import write_marks


documents_path = input("Documents path: ")
student_scores = get_docs(documents_path)

write_marks(student_scores[0], student_scores[1])


from pathlib import Path
from modules.getting_students_score import get_scores


def get_docs(documents_path):
    modified_path_name = documents_path.replace("\\", "/")
    path = Path(modified_path_name)

    student_percent_score_text = []
    for file in path.glob("*.docx"):
        student_document_path = modified_path_name + "/" + file.name
        student_percent_score_text.append(get_scores(student_document_path, file.name))

    return student_percent_score_text, modified_path_name

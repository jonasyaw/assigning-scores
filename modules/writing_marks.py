def write_marks(marks, path):
    # marks will contain a list of marks of the student,
    path_name = path + "/scores.txt"
    write_document = open(path_name, "w+")
    # a score.txt will contain the list of the scores. if it's not
    # available , it will be created in the folder, w+ allows reading
    # and writing privilleges on it.
    write_document.write("\n".join(marks))
    # the list is transformed to a string with every student's marks
    # on a new line.
    write_document.close()

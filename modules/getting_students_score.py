import docx
import re


def get_scores(document_path, filename):
    document = docx.Document(document_path)
    score = 0
    for para in document.paragraphs:
        marks_and_score_search = re.search(r"\d marks", para.text)
        if marks_and_score_search is not None:
            marks_and_score = marks_and_score_search.group().split(" ")
            score += float(marks_and_score[0])

    student_percent_score = (score / 50) * 100
    score_string = "{} - {} marks = {:.2f}%"

    return score_string.format(filename, score, student_percent_score)
